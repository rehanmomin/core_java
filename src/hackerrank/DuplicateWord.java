package hackerrank;
import java.util.HashSet;
import java.util.Set;

public class DuplicateWord {

	public static void main(String[] args) {
		String a = "rehan momin";
		
		char[] c = a.toCharArray();
		Set<Character> str = new HashSet<Character>();
		
		for (char d : c) {
			
			if(!str.add(d)) {
				System.out.println("Duplicate String : "+ d);
				
			}
			
		}
		
		

	}

}
