package hackerrank;
import java.util.Scanner;

public class DivisibleSumPairs {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int k = scan.nextInt();

		int[] ar = new int[n];

		for (int i = 0; i < ar.length; i++) {
			ar[i] = scan.nextInt();
		}


		int count = 0;

		for (int i = 0; i < ar.length - 1; i++) {
			for (int j = 1; j < ar.length; j++) {
				if(i < j) {
					if((ar[i]+ar[j]) % k ==0) {
						
						count ++;
					}
				}
			}
		}

		System.out.println(count);
	}
}

