package hackerrank;
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class BreakingTheRecords {

    // Complete the breakingRecords function below.
    static int[] breakingRecords(int[] scores) {
    	
    	int[] count = new int[2]  ; 
    	int highestScore = scores[0] ; 
    	int highRecordCount = 0;
    	int lowRecordCount = 0;
    	int lowestScore = scores[0] ;
    	
    	
    	for (int i = 0; i < scores.length; i++) {
    		
    		   		
			if(scores[i] > highestScore ) {
				highestScore = scores[i];
				highRecordCount++;
			}
			
			if(scores[i] < lowestScore ) {
				lowestScore = scores[i];
				lowRecordCount++;
			}
				
		}
    	
    	count[0] = highRecordCount;
    	count[1] = lowRecordCount;
		return count;


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] scores = new int[n];

        String[] scoresItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int scoresItem = Integer.parseInt(scoresItems[i]);
            scores[i] = scoresItem;
        }

        int[] result = breakingRecords(scores);

        for (int i = 0; i < result.length; i++) {
           System.out.print(result[i] + " ");
        }


        scanner.close();
    }
}
