package javabasics;

public class PassByValue {

	//Methods and local Variables are stores under stack memory
	//Objects and instance Variables are stores under stack memory
	public static void main(String[] args) {
		long id = 111;
		Report rep = new Report();

		incrementId(id);  // passing mrimitive variable
		System.out.println("after calling incrementId: "+ id);

		updateTitle1(rep); // passing reference variable
		System.out.println("After calling updateTitle1: " + rep.title);

		updateTitle2(rep); // passing reference variable
		System.out.println("After calling updateTitle1: " + rep.title);
	}

	private static void updateTitle2(Report rep) {
		rep.title = "updated";

	}

	private static void updateTitle1(Report rep) {
		rep = new Report();
		rep.title = "copy";
	}

	private static void incrementId(long id) {
		id = id +1 ;

	}

}

