package javabasics;

public class Variables {

	static int a = 20 ; //instance or class variable
	void method1() {
		int a = 10 ; // local variable	
		System.out.println("local variable : "+a);
		System.out.println("instance variable : "+this.a);
		this.a = 30 ;	
	}

	void method2() {
		System.out.println("instance variable : "+a);

	}
	public static void main(String[] args) {
		Variables demo1 = new Variables();
		demo1.method1();
		demo1.method2();
		
		Variables demo2 = new Variables();
		demo2.method2();

	}

}

// Each object of class creates copy of instance variable if static keyword is not used.
// Each object of class shares the same copy of instance variable if static keyword is used.