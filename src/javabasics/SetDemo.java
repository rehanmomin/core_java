package javabasics;
import java.util.HashSet;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		String word = "rehan momin";
		char[] charArray = word.toCharArray();
		Set<Character> s = new HashSet<Character>();
		
		int count = 0;
		for (int i = 0; i < charArray.length; i++) {
			if(!s.add(charArray[i])) {
				System.out.println("Duplicate char at position : "+i);
				count ++;
			}
		}
		
		System.out.println("Total number of duplicates:" +count);

	}

}
